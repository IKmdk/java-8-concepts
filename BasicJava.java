import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class BasicJava {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		BufferedReader readerObj=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter something....");
		String Variable=readerObj.readLine();
		System.out.println("BufferedReader....");
		System.out.println("value.... "+Variable);
		
		System.out.println("Enter number....");
		int IntVariable=Integer.parseInt(readerObj.readLine());
		System.out.println("BufferedReader....");
		System.out.println("value.... "+IntVariable);
		
		Scanner in=new Scanner(System.in);
		String variA;
		int variB;
		float variC;
		
		System.out.println("Enter something....");
		variA=in.nextLine();
		System.out.println("Enter int....");
		variB=in.nextInt();
		System.out.println("Enter float....");
		variC=in.nextFloat();
		System.out.println("Scanner....");
		System.out.println("value.... "+variA);
		System.out.println("value.... "+variB);
		System.out.println("value.... "+variC);
		in.close();
		

	}

}
